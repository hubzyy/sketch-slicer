<img width="96" src="./icon.png">

# Sketch Slicer

Sketch plugin for creating slices for selected layers

比较好用的切图插件，我看GitHub都不维护了有点可惜，索性就我来维护一下吧

<img width="400" src="./intro.png">

### 源码编译
首先你的有node环境，然后下载项目文件或者git clone项目
```
//安装依赖
npm install

//开发调试热更新
npm run watch

//项目打包
npm run build
```

### ⏬ 插件下载

\- [进入下载页面](https://gitee.com/hubzyy/sketch-slicer/releases)

\- 下载 `slice.sketchplugin.zip`

\- 解压缩

\- 双击`slice.sketchplugin` 即可安装



👇下面是介绍

## How to use
- Select one or more layers
- Run one of the 2 commands: Slice with padding or Slice with frame

## Options

#### Slice with Padding

Padding is the padding of the slice created from selected layers.

Enter one number to have equal padding from all sides, or manually set each of them using order similar to padding shorthand in CSS. For example, setting padding to `24/48` is equivalent to
```
padding: {
    top: 24
    right: 48
    bottom: 24
    left: 48
}
```
and setting padding to `12/14/16` is equivalent to
```
padding: {
    top: 12
    right: 14
    bottom: 16
    left: 14
}
```

Setting padding will not affect the selected layers' position.

#### Slice with Frame

Frame is the dimension of the slice. Using frame, the resulting slice layer will be centered around the selected layer.

Set one number will create a square slice layer, set two numbers to create a rectangle.

```
// 36;48
frame: {
  width: 36,
  height: 48
}

// 36
frame: {
  width: 36,
  height: 36
}
```

Note: If the selected layer doesn't have a rounded dimension (i.e `12.4241 x 14.1241`), setting frame will slightly adjust its position so that the result slice layer is placed on a round pixel and have selected layer centered inside.

#### Select preset

Select one of the presets that you set in Sketch Preference `cmd + , `

#### Create symbol

Create symbol out of the selected layers.




###### TODO
- [x] Set padding for slice
- [x] Set fixed slice size
- [ ] Option to break text inside selected layers
- [x] ~~Set export options~~ Use user's export preset (set it Preference)
- [x] Create symbol with selected layer
  - [ ] If selected layer contains symbol, have an option to detach that nested symbol
