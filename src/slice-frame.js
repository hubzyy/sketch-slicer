import {slice,setting} from './slice'

export default function (context) {
  const doc = context.document;
  let selected = context.selection;
  if (selected.length == 0){
    doc.showMessage('select at least one layer.')
    return
  }

  let option = setting('frame', selected.length);
  if (option != undefined) {
  selected.forEach(layer => {
    slice(layer, option);
  })
  doc.showMessage('sliced ' + selected.length + ' layer(s).');
  }
}
